-- Name: Lilac Theme
local theme = {}

theme.setup = function()
  vim.cmd("highlight clear")
  vim.o.background = "dark"
  vim.g.colors_name = "lilac"

  local highlights = {
    -- Enable transparency
    Normal = { bg = "NONE", fg = "#cfc2db" },
    NonText = { bg = "NONE", fg = "#533b68" },
    Cursor = { fg = "#160e1c", bg = "#b089f2" },
    Comment = { fg = "#704d8a", italic = true },
    Constant = { fg = "#f287c2" },
    Identifier = { fg = "#b089f2" },
    Statement = { fg = "#e087f2" },
    PreProc = { fg = "#ffa3e4" },
    Type = { fg = "#d8b4f0" },
    Special = { fg = "#ff94d8" },
    Underlined = { fg = "#dcb3ff", underline = true },
    Error = { fg = "#ff667a", bg = "NONE" },
    Visual = { bg = "#3c294a" },
    LineNr = { fg = "#7a5b90" },
    CursorLineNr = { fg = "#e0d6eb" },
    VertSplit = { fg = "#533b68" },
    StatusLine = { fg = "#cfc2db", bg = "#3c294a" },
    StatusLineNC = { fg = "#704d8a", bg = "#22172b" },
    Pmenu = { fg = "#cfc2db", bg = "#30203b" },
    PmenuSel = { fg = "#ffffff", bg = "#704d8a" },
    Search = { fg = "#ffffff", bg = "#704d8a" },
    MatchParen = { fg = "#ffffff", bg = "#9468b5" },
    ErrorMsg = { fg = "#ff667a", bg = "NONE" },
    WarningMsg = { fg = "#ff90d0", bg = "NONE" },
    Folded = { fg = "#7a5b90", bg = "#3c294a" },
    EndOfBuffer = { fg = "NONE" },
  }

  for group, opts in pairs(highlights) do
    vim.api.nvim_set_hl(0, group, opts)
  end
end

return theme

