HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt autocd beep extendedglob nomatch notify correct_all hist_ignore_all_dups hist_append

# Keybind snippet from https://bbs.archlinux.org/viewtopic.php?id=26110
autoload zkbd
[[ ! -f ${ZDOTDIR:-$HOME}/.zkbd/$TERM-${${DISPLAY:t}:-$VENDOR-$OSTYPE} ]] && zkbd
source ${ZDOTDIR:-$HOME}/.zkbd/$TERM-${${DISPLAY:t}:-$VENDOR-$OSTYPE}

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char

# Load version control information
autoload -Uz vcs_info
precmd() { vcs_info }

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats 'on %F{4}branch %F{6}%b '
 
autoload -Uz compinit
compinit

# Setup the left prompt, including VCS info
setopt PROMPT_SUBST
PROMPT='%F{9}%n%F{1}%(!.#.) %F{15}in %F{3}${PWD/#$HOME/~} %F{15}${vcs_info_msg_0_}%F{2}%#%F{15} > '
RPROMPT='%(?.%F{2}✓.%F{1}✗ %?)'

# Setup some basic aliases, and add color where possible
alias ...='cd ../..'
alias ....='cd ../../..'
alias dir='cd $1 && ls'
alias ls='ls --color'
alias la='ls -a --color'
alias ll='ls -lah --color'
alias grep='grep --color'
alias ip='ip -c'
alias tar='tar --color=auto'
alias df='df --human-readable --color=always'
alias du='du --human-readable --color=always'
alias diff='diff --color=auto'
alias tree='tree -C'
alias udb='sudo updatedb'
alias clr='clear'
alias rcd='ranger --choosedir=$HOME/.rangerdir; LASTDIR=`cat $HOME/.rangerdir`; cd "$LASTDIR"'
alias please='sudo $(fc -ln -1)'
alias updatemirrors='sudo reflector --sort rate --verbose --country US  --protocol https --save /etc/pacman.d/mirrorlist'
alias rsync='rsync --info=progress2 --human-readable --color=always'
alias dotfiles='git --git-dir=$HOME/.dotfiles --work-tree=$HOME' >> ~/.zshrc

# Run fetch if in terminal
[[ "$(cat /proc/$PPID/comm)" =~ "(kitty)" ]] && hyfetch

### Added by Zinit's installer
if [[ ! -f $HOME/.local/share/zinit/zinit.git/zinit.zsh ]]; then
    command mkdir -p "$HOME/.local/share/zinit" && command chmod g-rwX "$HOME/.local/share/zinit"
    command git clone https://github.com/zdharma-continuum/zinit "$HOME/.local/share/zinit/zinit.git" && \
        print -P "%F{33} %F{34}Installation successful.%f%b" || \
        print -P "%F{160} The clone has failed.%f%b"
fi

source "$HOME/.local/share/zinit/zinit.git/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    zdharma-continuum/zinit-annex-as-monitor \
    zdharma-continuum/zinit-annex-bin-gem-node \
    zdharma-continuum/zinit-annex-patch-dl \
    zdharma-continuum/zinit-annex-rust

### End of Zinit's installer chunk

zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-history-substring-search

typeset -A ZSH_HIGHLIGHT_STYLES

# Setting syntax highlight color overrides
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=#FF667A'
ZSH_HIGHLIGHT_STYLES[command]='fg=#B2FFE6'
